# Binar: Challenge

Di dalam challenge ini terdapat implementasi authentication dan authorization di dalamnya.

## Getting Started

Pertama kali lakukan install, dengan cara :

```sh
npm install
```

Lakukan setingan .env seperti dibawah ini :

```sh
DB_USERNAME=postgres
DB_PASSWORD=12345
DB_NAME=database
DB_HOST=127.0.0.1
PORT=8000
SUPERADMIN_PASSWORD=admin1234
JWT_PRIVATE_KEY=xxxx
```

```sh
npm install
```

Untuk menjalankan development server, kalian tinggal jalanin salah satu script di package.json, yang namanya `develop`.

```sh
npm run develop
```

## Database Management

Di dalam repository ini sudah terdapat beberapa script yang dapat digunakan dalam memanage database, yaitu:

- `sequelize db:create` digunakan untuk membuat database
- `sequelize db:drop` digunakan untuk menghapus database
- `sequelize db:migratee` digunakan untuk menjalankan database migration
- `sequelize db:seed:all` digunakan untuk melakukan seeding
- `sequelize db:migrate:undo` digunakan untuk membatalkan migrasi terakhir

## user Admin & SuperAdmin

Di dalam repository sudah terdapat user superadmin.
