const express = require("express");
const controllers = require("../app/controllers");
const middlewares = require("../app/middlewares");
const apiRouter = express.Router();
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("../data/swagger.json");

apiRouter.use("/api-docs", swaggerUi.serve);
apiRouter.get("/api-docs", swaggerUi.setup(swaggerDocument));

apiRouter.post(
  "/api/v1/register",
  middlewares.conditionalchecking.conditionalChecking,
  controllers.api.v1.userController.register
);

apiRouter.post(
  "/api/v1/login",
  middlewares.checkdata.checkData,
  controllers.api.v1.userController.login
);

apiRouter.get(
  "/api/v1/users/:id",
  middlewares.authorization.authorize,
  controllers.api.v1.userController.getUserData
);

apiRouter.get(
  "/api/v1/users",
  middlewares.authorization.authorize,
  controllers.api.v1.userController.getAllUsers
);

apiRouter.get(
  "/api/v1/whoami",
  middlewares.authorization.authorize,
  controllers.api.v1.userController.whoAmI
);

apiRouter.put(
  "/api/v1/users/update-role/:id",
  middlewares.authorization.checkSuperAdmin,
  controllers.api.v1.userController.ifSuperAdmin
);

apiRouter.post(
  "/api/v1/cars",
  middlewares.authorization.checkAdmin,
  controllers.api.v1.carController.create
);

apiRouter.get(
  "/api/v1/cars",
  middlewares.authorization.authorize,
  controllers.api.v1.carController.list
);

apiRouter.get(
  "/api/v1/cars/:id",
  middlewares.authorization.authorize,
  controllers.api.v1.carController.showcarbyid
);

apiRouter.put(
  "/api/v1/cars/:id",
  middlewares.authorization.checkAdmin,
  controllers.api.v1.carController.updatecar
);

apiRouter.delete(
  "/api/v1/cars/:id",
  middlewares.authorization.checkAdmin,
  controllers.api.v1.carController.destroycar
);
// /**
//  * TODO: Implement your own API
//  *       implementations
//  */
// apiRouter.get("/api/v1/posts", controllers.api.v1.postController.list);
// apiRouter.post("/api/v1/posts", controllers.api.v1.postController.create);
// apiRouter.put("/api/v1/posts/:id", controllers.api.v1.postController.update);
// apiRouter.get("/api/v1/posts/:id", controllers.api.v1.postController.show);
// apiRouter.delete(
//   "/api/v1/posts/:id",
//   controllers.api.v1.postController.destroy
// );

// /**
//  * TODO: Delete this, this is just a demonstration of
//  *       error handler
//  */
// apiRouter.get("/api/v1/errors", () => {
//   throw new Error(
//     "The Industrial Revolution and its consequences have been a disaster for the human race."
//   );
// });

// apiRouter.use(controllers.api.main.onLost);
// apiRouter.use(controllers.api.main.onError);

module.exports = apiRouter;
