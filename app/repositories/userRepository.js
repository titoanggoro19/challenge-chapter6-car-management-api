const { User } = require("../models");

module.exports = {
  create(inputData) {
    return User.create(inputData);
  },
  update(id, updatedData) {
    return User.update(updatedData, {
      where: {
        id,
      },
    });
  },
  findOne(key) {
    return User.findOne(key);
  },
  find(id) {
    return User.findByPk(id);
  },

  findAll() {
    return User.findAll();
  },
  
  getTotalUsers() {
    return User.count();
  },
};
