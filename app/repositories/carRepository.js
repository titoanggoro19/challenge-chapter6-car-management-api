const { Car } = require("../models");

module.exports = {
  create(inputData) {
    return Car.create(inputData);
  },
  update(id, updateArgs) {
    return Car.update(updateArgs, {
      where: {
        id,
      },
    });
  },
  delete(id, updateArgs) {
    return Car.update(updateArgs, {
      where: {
        id,
      },
    });
  },
  findOne(key) {
    return Car.findOne(key);
  },
  find(id) {
    return Car.findByPk(id);
  },
  findAll(args) {
    return Car.findAll(args);
  },

  getTotalCar(args) {
    return Car.count(args);
  },
};
