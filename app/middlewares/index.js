const conditionalchecking = require("./conditionalchecking");
const checkdata = require("./checkdata");
const authorization = require("./authorization");

module.exports = {
  conditionalchecking,
  checkdata,
  authorization,
};
