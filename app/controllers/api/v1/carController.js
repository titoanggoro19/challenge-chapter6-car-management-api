const carService = require("../../../services/carService");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

module.exports = {
  async create(req, res) {
    req.body.createdBy = req.user.email;

    carService
      .create(req.body)
      .then((createdCar) => {
        res.status(201).json({
          status: "Success",
          message: `Car Successfully Created by ${req.user.email}`,
          data: createdCar,
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  async list(req, res) {
    carService
      .list({
        where: { isDeleted: false },
      })
      .then(({ data, count }) => {
        res.status(200).json({
          status: "OK",
          data: { cars: data },
          meta: { total: count },
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  async showcarbyid(req, res) {
    carService
      .get(req.params.id)
      .then((cars) => {
        res.status(200).json({
          status: "OK",
          data: cars,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  async updatecar(req, res) {
    req.body.updatedBy = req.user.email;
    carService
      .update(req.params.id, req.body)
      .then(() => {
        res.status(200).json({
          status: "OK",
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  async destroycar(req, res) {
    carService
      .delete(req.params.id, { isDeleted: true, deleteBy: req.user.email })
      .then((car) => {
        res.status(200).json({
          deleteBy: req.user.email,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },
};
