const usersRepository = require("../repositories/userRepository");

module.exports = {
  create(requestBody) {
    return usersRepository.create(requestBody);
  },
  update(id, requestBody) {
    return usersRepository.update(id, requestBody);
  },

  async list() {
    try {
      const users = await usersRepository.findAll();
      const usersCount = await usersRepository.getTotalUsers();

      return {
        data: users,
        count: usersCount,
      };
    } catch (err) {
      throw err;
    }
  },

  getOne(key) {
    return usersRepository.findOne(key);
  },
  get(id) {
    return usersRepository.find(id);
  },
};
