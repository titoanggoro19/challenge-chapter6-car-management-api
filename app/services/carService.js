const carRepository = require("../repositories/carRepository");

module.exports = {
  create(requestBody) {
    return carRepository.create(requestBody);
  },
  update(id, requestBody) {
    return carRepository.update(id, requestBody);
  },
  delete(id, requestBody) {
    return carRepository.delete(id, requestBody);
  },
  getOne(key) {
    return carRepository.findOne(key);
  },
  async list(args) {
    try {
      const cars = await carRepository.findAll(args);
      const carCount = await carRepository.getTotalCar(args);

      return {
        data: cars,
        count: carCount,
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return carRepository.find(id);
  },
};
