'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Car extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Car.init({
    model: DataTypes.STRING,
    type: DataTypes.STRING,
    year: DataTypes.STRING,
    image: DataTypes.STRING,
    rentPerDay: DataTypes.STRING,
    description: DataTypes.STRING,
    transmission: DataTypes.STRING,
    available: DataTypes.BOOLEAN,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING,
    isDeleted: DataTypes.BOOLEAN,
    deleteBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Car',
  });
  return Car;
};